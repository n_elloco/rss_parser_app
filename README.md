# RSS parser application

## Launch in docker

1. Build image and run container

```bash
$ docker build -t rss_parser .
$ docker run -d -it -p 8080:8080 rss_parser
```

2. Create user for admin panel

```bash
$ docker exec -it <CONTAINER_ID> python manage.py createsuperuser
```

3. Go to admin panel http://localhost:8080/admin/

4. Go to page Posts and push Upload RSS button

## Testing

```bash
$ python manage.py test
```

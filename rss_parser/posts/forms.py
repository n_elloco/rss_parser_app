from django import forms

from .models import Source


class RSSFeedLoadForm(forms.Form):
    """Form for RSS uploading"""
    source = forms.ModelChoiceField(queryset=Source.objects.all())

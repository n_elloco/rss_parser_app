from django.conf.urls import url
from django.contrib import admin
from django.contrib.admin.helpers import AdminForm
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse

from data_mapper.exceptions import InvalidDataSource

from .forms import RSSFeedLoadForm
from .importer import upload_rss
from .models import Post, Source


class PostsModelAdmin(admin.ModelAdmin):
    """Admin manager for posts"""
    list_display = ('title', 'source', 'author', 'published')
    ordering = ('-published',)
    list_filter = ('source',)

    def get_urls(self):
        return [
            url(
                r'load_rss/$',
                self.admin_site.admin_view(self.load_rss),
                name='posts_load_rss',
            ),
        ] + super().get_urls()

    def load_rss(self, request, form_url=''):
        """ View for uploading RSS """

        messages = []

        if request.method == 'POST':
            form = RSSFeedLoadForm(request.POST, request.FILES)
            if form.is_valid():
                try:
                    upload_rss(form.data['source'])
                except InvalidDataSource as ex:
                    form.add_error(
                        'source', 'Data source is not available: %s' % ex)
                else:
                    return HttpResponseRedirect(
                        reverse(
                            '%s:posts_post_changelist' % (
                                self.admin_site.name,
                            )
                        )
                    )
        else:
            form = RSSFeedLoadForm()

        fieldsets = [(None, {'fields': list(form.base_fields)})]
        admin_form = AdminForm(form, fieldsets, {})

        context = {
            'title': 'Upload RSS',
            'adminForm': admin_form,
            'form': form,
            'add': True,
            'opts': self.model._meta,
            'messages': messages,
        }
        context.update(self.admin_site.each_context(request))
        request.current_app = self.admin_site.name

        return TemplateResponse(
            request, 'admin/posts/load_rss.html', context
        )


admin.site.register(Post, PostsModelAdmin)
admin.site.register(Source)

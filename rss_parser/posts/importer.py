import datetime

from django.core.exceptions import FieldDoesNotExist
from django.db.models import DateField
from django.utils.dateparse import parse_datetime

from data_mapper.mapper import BaseMapper

from .models import Post, Source, Author, Tag


class BaseImporter(object):
    """Base class for data import with data mapper"""
    mapper_cls = None
    model = None

    def __init__(self, source_id):
        self.source = Source.objects.get(pk=source_id)

    def upload(self):
        """Uploads data"""
        config = self.source.get_config_path()
        data = self.mapper_cls(config).convert()
        for item in data:
            try:
                instance = self.model.objects.get(
                    source=self.source,
                    external_id=item['external_id']
                )
            except self.model.DoesNotExist:
                instance = self.model(source=self.source)

            for key, value in item.items():
                try:
                    field = self.model._meta.get_field(key)
                except FieldDoesNotExist:
                    continue
                else:
                    if isinstance(field, DateField):
                        value = str2datetime(value)
                    setattr(instance, key, value)

            instance.save()
            self.postprocess_instance(instance, item)

    def postprocess_instance(self, instance, data):
        """Post-actions"""
        pass


class PostImporter(BaseImporter):

    mapper_cls = BaseMapper
    model = Post

    def postprocess_instance(self, instance, data):
        if 'author_external_id' in data:
            try:
                author = Author.objects.get(
                    source=self.source,
                    external_id=data['author_external_id']
                )
            except Author.DoesNotExist:
                author = Author.objects.create(
                    source=self.source,
                    external_id=data['author_external_id'],
                    name=data.get('author_name', data['author_external_id'])
                )
            instance.author = author

        if 'tag' in data:
            tags = data['tag']
            if not isinstance(tags, list):
                tags = [tags]
            instance.tags.clear()
            for tag_name in tags:
                try:
                    tag = Tag.objects.get(name=tag_name)
                except Tag.DoesNotExist:
                    tag = Tag.objects.create(name=tag_name)
                instance.tags.add(tag)

        instance.save()


def upload_rss(source_id):
    """Calls uploading of RSS via PostImporter"""
    PostImporter(source_id).upload()


def str2datetime(date_string):
    """Convert string with date to datetime object

    :param str date_string:
    """
    formats = (
        '%a, %d %b %Y %X GMT',
        '%c',
    )
    value = parse_datetime(date_string)
    if value:
        return value

    for _format in formats:
        try:
            value = datetime.datetime.strptime(date_string, _format)
        except ValueError:
            continue
        else:
            return value

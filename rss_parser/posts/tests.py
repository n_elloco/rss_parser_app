import datetime
import pytz
from unittest import TestCase, mock

from .importer import PostImporter
from .models import Post


class PostImporterTestCase(TestCase):

    @mock.patch('posts.importer.BaseMapper.convert')
    def test_upload(self, m_convert):
        importer = PostImporter(source_id=1)
        post_item = {
            'external_id': 'ext-1',
            'title': 'Some title',
            'published': 'Mon, 30 Jul 2018 21:29:51 GMT',
            'description': 'Some description',
            'url': 'http://site.com/123',
            'tag': ['tag1', 'tag2'],
            'author_external_id': 'ext-a-1',
        }
        post_fields = ('external_id', 'title', 'description', 'url')
        m_convert.return_value = [post_item]
        importer.upload()

        post = Post.objects.get(source_id=1, external_id='ext-1')
        for field in post_fields:
            self.assertEqual(post_item[field], getattr(post, field))
        self.assertEqual(
            post.published,
            datetime.datetime(2018, 7, 30, 21, 29, 51, tzinfo=pytz.UTC))
        self.assertEqual(post.author.external_id,
                         post_item['author_external_id'])
        self.assertListEqual(
            list(post.tags.values_list('name', flat=True)), post_item['tag'])

        post_item = {
            'external_id': 'ext-1',
            'title': 'New title',
            'published': 'Mon, 30 Jul 2018 21:29:51 GMT',
            'description': 'New description',
            'url': 'http://site.com/123',
            'tag': ['tag1', 'tag3'],
            'author_external_id': 'ext-a-2',
        }
        m_convert.return_value = [post_item]
        importer.upload()
        post = Post.objects.get(source_id=1, external_id='ext-1')
        for field in post_fields:
            self.assertEqual(post_item[field], getattr(post, field))
        self.assertListEqual(
            list(post.tags.values_list('name', flat=True)), post_item['tag'])
        self.assertEqual(post.author.external_id,
                         post_item['author_external_id'])

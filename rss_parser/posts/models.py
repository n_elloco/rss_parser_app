import os

from django.db import models

MAPPINGS_FOLDER = 'mappings'


class Source(models.Model):
    """ Model for sources of RSS feeds """
    name = models.CharField('Name', max_length=50)
    config_name = models.CharField('Config Name', max_length=50)

    class Meta:
        db_table = 'posts_sources'

    def __str__(self):
        return self.name

    def get_config_path(self):
        """Returns source config path"""
        return os.path.join(
            os.path.dirname(__file__), MAPPINGS_FOLDER, self.config_name)


class Author(models.Model):
    """ Model for posts authors """
    name = models.TextField('Name', default='')
    external_id = models.CharField('External ID', max_length=50)
    source = models.ForeignKey(Source, on_delete=models.PROTECT)

    class Meta:
        db_table = 'posts_authors'
        index_together = 'external_id', 'source'

    def __str__(self):
        return self.name


class Tag(models.Model):
    """ Model for posts tags """
    name = models.TextField('Name', default='')

    class Meta:
        db_table = 'posts_tags'

    def __str__(self):
        return self.name


class Post(models.Model):
    """ Model for posts """
    title = models.TextField('Title', default='')
    description = models.TextField('Description', default='')
    published = models.DateTimeField('Published')
    author = models.ForeignKey(Author, on_delete=models.PROTECT, null=True)
    external_id = models.CharField('External ID', max_length=50)
    url = models.URLField(default='')
    source = models.ForeignKey(Source, on_delete=models.PROTECT)
    tags = models.ManyToManyField(to=Tag, related_name='posts')

    class Meta:
        db_table = 'posts_posts'
        index_together = 'external_id', 'source'

    def __str__(self):
        return self.title

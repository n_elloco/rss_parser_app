FROM python:3.6.6

RUN apt-get update \
        && apt-get -y upgrade \
        && apt-get install -y \
            python3-dev python3-pip \
            build-essential libpcre3 \
            libpcre3-dev libpq-dev

RUN mkdir /src

COPY requirements.txt /src/requirements.txt
RUN pip install -Ur /src/requirements.txt

COPY rss_parser /src/rss_parser

WORKDIR /src/rss_parser

EXPOSE 8080

CMD python manage.py migrate && python manage.py runserver 0.0.0.0:8080
